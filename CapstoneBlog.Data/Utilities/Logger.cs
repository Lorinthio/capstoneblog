﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CapstoneBlog.Data.Utilities
{
    public static class Logger
    {

        public static void LogErrors(Exception e)
        {

            string path = @"C:\_repos\capstoneblog\CapstoneBlog\Content\Log.txt";

            string prefix = "\n[" + DateTime.Now.ToString("HH:MM:ss") + "]";
            File.AppendAllText(path, prefix + e.ToString());

        }

    }
}