﻿using CapstoneBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneBlog.Data
{
    public interface IRepository
    {
        //Posts
        Post GetPostById(int id);
        PostVM GetPostVMById(int id);
        List<PostVM> GetAllPublicPosts();
        List<PostVM> GetAuthorsPosts(string authorAuth);
        List<PostVM> GetAllPostsForRevision();
        List<PostVM> GetPostsWithCategories(List<string> categories);
        List<PostVM> GetAllDeletedPosts();
        List<PostVM> GetAllStaticPages();
        void AddPost(Post Post);
        void SetApproved(int postID);
        void SetRejected(int postID);
        void SetDeleted(int postID);



        //Categories
        List<Category> GetAllCategories();

        //Users
        AspNetUser GetUserByAuth(string auth);
        List<AspNetUser> GetAllUsersWithRole(int roleId);
        bool UsernameAvailable(string username);
        bool CategoryExists(string category);
        Category GetCategory(string category);
        void UpdatePost(PostVM post);
        void AddCategory(string category);
        List<UserVM> GetAllUsers();
        void UpdateUserRole(string id, string role);
    }
}
