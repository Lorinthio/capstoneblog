﻿using System;
using System.Collections.Generic;
using System.Linq;
using CapstoneBlog.Models;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneBlog.Data
{
    public class DataRepository : IRepository
    {
        public bool CategoryExists(string category)
        {
            using (var entities = new Entities())
            {
                return entities.Categories.Any(c => c.Category1.ToLower() == category.ToLower());
            }
            
        }

        public Category GetCategory(string category)
        {
            using (var entities = new Entities())
            {
                var categories = entities.Categories.Where(c => c.Category1.ToLower() == category.ToLower());
                return categories.FirstOrDefault();
            }
                        
        }

        public List<Category> GetAllCategories()
        {
            using (var entities = new Entities())
            {
                var categories = entities.Categories.OrderBy(c => c.Category1);

                return categories.ToList();

            }
                
        }

        public List<PostVM> GetAllDeletedPosts()
        {
            List<PostVM> vms = new List<PostVM>();
            using (var entities = new Entities())
            {
                var posts = entities.Posts.Where(r => r.isDeleted == true);
                foreach(Post p in posts)
                {
                    vms.Add(new PostVM(p));
                }
            }
            return vms;
        }

        public List<PostVM> GetAllPostsForRevision()
        {
            List<PostVM> vms = new List<PostVM>();
            using (var entities = new Entities())
            {
                var posts = entities.Posts.Where(r => r.isApproved == false && !r.isRejected);
                foreach(Post p in posts)
                {
                    vms.Add(new PostVM(p));
                }
            }
            return vms;
             
        }

        public List<PostVM> GetAllPublicPosts()
        {
            var vms = new List<PostVM>();
            //CheckForExpiration();
            using (var entities = new Entities())
            {
                //Get all posts that are approved and not deleted
                var posts = entities.Posts.Where(p => !p.isDeleted && !p.isStaticPage && p.isApproved && (p.Expiration == null || p.Expiration > DateTime.Now)).OrderByDescending(p => p.Date);
                //var posts = entities.Posts.OrderByDescending(p => p.Date);
                foreach(Post p in posts)
                {
                    vms.Add(new PostVM(p));
                }
            }
            return vms;
        }

        public void CheckForExpiration()
        {
            using (var entities = new Entities())
            {
                DateTime now = DateTime.Now;
                foreach(Post p in entities.Posts)
                {
                    if(p.Date < now)
                    {
                        p.isDeleted = true;
                    }
                }
            }
        }

        public List<AspNetUser> GetAllUsersWithRole(int roleId)
        {
            using (var entities = new Entities())
            {
                var users = entities.AspNetUsers.Where(m => m.AspNetRoles.Any(r => r.Id == roleId.ToString()));

                return users.ToList();

            }
                
        }

        public List<PostVM> GetAuthorsPosts(string authorAuth)
        {
            List<PostVM> vms = new List<PostVM>();
            using (var entities = new Entities())
            {
                var posts = entities.Posts.Where(p => p.Id == authorAuth);
                foreach (Post p in posts)
                {
                    vms.Add(new PostVM(p));
                }
            }
            return vms;
        }

        public Post GetPostById(int id)
        {
            using (var entities = new Entities())
            {
                var post = entities.Posts.Where(p => p.Post_ID == id);

                return post.FirstOrDefault();
            }
        }

        public PostVM GetPostVMById(int id)
        {
            using (var entities = new Entities())
            {
                var post = entities.Posts.Where(p => p.Post_ID == id);

                return new PostVM(post.FirstOrDefault());
            }
        }

        public List<PostVM> GetPostsWithCategories(List<string> categories)
        {
            List<PostVM> vms = new List<PostVM>();
            using (var entities = new Entities())
            {
                var posts = entities.Posts.Where(m => categories.All(i => m.Categories.Any(c => c.Category1 == i))).OrderByDescending(p => p.Date);
                foreach (Post p in posts)
                {
                    vms.Add(new PostVM(p));
                }
            }
            return vms;
        }

        public AspNetUser GetUserByAuth(string auth)
        {
            using (var entities = new Entities())
            {
                var user = entities.AspNetUsers.Where(m => m.Id == auth);

                return user.FirstOrDefault();

            }     

        }

        public bool UsernameAvailable(string username)
        {
            using (var entities = new Entities())
            {
                if (entities.AspNetUsers.Where(m => m.UserName == username).Count() >= 1)
                {
                    return false;
                }
                return true;

            }
         
        }

        public void AddPost (Post post)
        {
            using (var entities = new Entities())
            {
                Post p = new Post();
                foreach(Category c in post.Categories)
                {
                    p.Categories.Add(entities.Categories.FirstOrDefault(c2 => c2.Category1.ToLower() == c.Category1.ToLower()));
                }
                post.Categories = p.Categories;
                entities.Posts.Add(post);
                entities.SaveChanges();
            }
        }

        public void UpdatePost(PostVM post)
        {
            using(var entities = new Entities())
            {
                Post p = entities.Posts.FirstOrDefault(p2 => p2.Post_ID == post.PostID);
                p.Categories = new List<Category>();
                foreach (string c in post.Categories)
                {
                    p.Categories.Add(entities.Categories.FirstOrDefault(c2 => c2.Category1.ToLower() == c.ToLower()));
                }
                p.Title = post.Title;
                p.PostContent = post.PostContent;
                p.isStaticPage = post.isStaticPage;
                p.Expiration = post.Expiration;

                entities.Posts.Attach(p);
                var entry = entities.Entry(p);
                entry.State = System.Data.Entity.EntityState.Modified;
                entities.SaveChanges();
            }
        }

        public void SetApproved(int postID)
        {
            Post post = GetPostById(postID);
            post.isApproved = true;

            using (var entities = new Entities())
            {
                entities.Posts.Attach(post);
                var entry = entities.Entry(post);
                entry.State = System.Data.Entity.EntityState.Modified;
                entities.SaveChanges();
            }
        }


        public void SetRejected(int postID)
        {
            Post post = GetPostById(postID);
            post.isRejected = true;

            using (var entities = new Entities())
            {
                entities.Posts.Attach(post);
                var entry = entities.Entry(post);
                entry.State = System.Data.Entity.EntityState.Modified;
                entities.SaveChanges();

            }

        }


        public void SetDeleted(int postID)
        {
            Post post = GetPostById(postID);
            post.isDeleted = true;

            using (var entities = new Entities())
            {
                entities.Posts.Attach(post);
                var entry = entities.Entry(post);
                entry.State = System.Data.Entity.EntityState.Modified;
                entities.SaveChanges();

            }

        }

        public List<PostVM> GetAllStaticPages()
        {
            List<PostVM> vms = new List<PostVM>();
            using (var entities = new Entities())
            {
                var posts = entities.Posts.Where(p => p.isStaticPage && p.isApproved);
                foreach (Post p in posts)
                {
                    vms.Add(new PostVM(p));
                }
            }
            return vms;
        }

        public void AddCategory(string category)
        {
            using (var entities = new Entities())
            {
                Category c = new Category();
                c.Category1 = category;
                entities.Categories.Add(c);
                entities.SaveChanges();
            }
        }

        public List<UserVM> GetAllUsers()
        {
            List<UserVM> list = new List<UserVM>();

            using (var entities = new Entities())
            {
                var asplist = entities.AspNetUsers.OrderBy(u => u.UserName).ToList();
                foreach (AspNetUser User in asplist)
                {
                    UserVM userVM = new UserVM();
                    userVM.UserName = User.UserName;
                    userVM.AuthID = User.Id;
                    userVM.Role = User.AspNetRoles.FirstOrDefault().Name;
                    list.Add(userVM);
                }
            }

            return list;

        }

        public void UpdateUserRole(string id, string role)
        {

            using (var entities = new Entities())
            {
                var user = entities.AspNetUsers.FirstOrDefault(u => u.Id == id);
                var Role = entities.AspNetRoles.FirstOrDefault(r => r.Name == role);
                user.AspNetRoles.Clear();
                user.AspNetRoles.Add(Role);

                entities.AspNetUsers.Attach(user);
                var entry = entities.Entry(user);
                entry.State = System.Data.Entity.EntityState.Modified;
                entities.SaveChanges();

            }
        }

    }
}
