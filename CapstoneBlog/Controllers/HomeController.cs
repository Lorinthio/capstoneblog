﻿using CapstoneBlog.BLL;
using CapstoneBlog.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CapstoneBlog.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICBManager manager;

        public HomeController(ICBManager manager)
        {
            this.manager = manager;
        }

        public ActionResult Index()
        {
            List<PostVM> statics = manager.GetAllStaticPages();
            return View(statics);
        }

        public ActionResult GetPosts(int pos)
        {
            List<PostVM> posts = manager.GetAllPublicPosts();
            return PartialView("BlogTable", posts);
        }

        public ActionResult GetPostsByCategory(string category)
        {
            List<string> categories = category.Split(' ').ToList();
            List<PostVM> posts = manager.GetPostsWithCategories(categories);
            return PartialView("BlogTable", posts);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SavePost(PostVM vm)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();
                PostVM post;
                if(vm.PostID != 0)
                {
                    post = manager.GetPostVMById(vm.PostID);
                    post.Categories = new List<string>();
                    post.Title = vm.Title;
                    post.Expiration = vm.Expiration;
                    post.isStaticPage = vm.isStaticPage;
                    post.PostContent = vm.PostContent;
                    post.isRejected = false;
                    post.Date = DateTime.Now;
                    var stringList = manager.GetPostCategoriesAsStringList(post.PostContent);
                    manager.GetPostCategories(stringList);
                    post.Categories = stringList;
                    manager.UpdatePost(post);
                    return Admin();
                }
            }
            return GetPosts(0);
        }

        public ActionResult ReviseBlog(int postId)
        {
            PostVM post = manager.GetPostVMById(postId);
            return PartialView("ReviseBlog", post);
        }

        public ActionResult ReadBlog(int postId)
        {
            PostVM post = manager.GetPostVMById(postId);
            return PartialView("ReadBlog", post);
        }

        public ActionResult LoadStatic(int postId)
        {
            PostVM post = manager.GetPostVMById(postId);
            return PartialView("StaticPage", post);
        }

        public ActionResult Create()
        {
            return PartialView(new PostVM());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PostForReview(PostVM postvm)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();
                Post post = new Post();
                post.Title = postvm.Title;
                post.Expiration = postvm.Expiration;
                post.PostContent = postvm.PostContent;
                post.isStaticPage = postvm.isStaticPage;
                post.Id = userId;
                post.Date = DateTime.Now;
                post.isApproved = false;
                post.isDeleted = false;
                var stringList = manager.GetPostCategoriesAsStringList(post.PostContent);
                post.Categories = manager.GetPostCategories(stringList);
                manager.AddPost(post);
                return Admin();
            }
            return GetPosts(0);
        }

        [Authorize]
        public ActionResult Admin()
        {
            if (User.IsInRole("Editor"))
            {
                //Get all posts waiting for revision
                List<PostVM> posts = manager.GetAllPostsForRevision();
                posts = posts.OrderByDescending(p => p.Date).ToList();
                return PartialView("Admin", posts);
            }
            else if (User.IsInRole("Author"))
            {
                //Get the authors posts
                List<PostVM> posts = manager.GetAuthorsPosts(User.Identity.GetUserId());
                return PartialView("Admin", posts);
            }
            else
                return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SetPostApproved(int postID)
        {
            if (User.IsInRole("Editor"))
            {
                manager.SetApproved(postID);
                return Admin();
            }
            return GetPosts(0);
        }

        [HttpPost]
        public ActionResult SetPostRejected(int postID)
        {
            if (User.IsInRole("Editor"))
            {
                manager.SetRejected(postID);
                return Admin();
            }
            return GetPosts(0);
        }

        [HttpPost]
        public ActionResult SetPostDeleted(int postID)
        {
            if (User.IsInRole("Editor"))
            {
                manager.SetDeleted(postID);
                return Admin();
            }
            return GetPosts(0);
        }

        [Authorize(Roles = "Editor")]
        public ActionResult UserList()
        {            
            var list = manager.GetAllUsers();
            return PartialView("UserList", list);
        }

        [HttpPost]
        public ActionResult UpdateUserRole(string id, string role)
        {
            manager.UpdateUserRole(id, role);
            return null;
        }
    }
}