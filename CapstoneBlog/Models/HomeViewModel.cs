﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneBlog.Models
{
    public class HomeViewModel
    {
        public ApplicationUser User { get; set; }
        public List<Post> Posts { get; set; }
    }
}