﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Mvc;
using System;
using System.Web;
using System.IO;

namespace CapstoneBlog.Models
{
    public class SecurityAttribute : AuthorizeAttribute
    {
        // Set default Unauthorized Page Url here
        private string _notifyUrl = "/Error/Unauthorized";

        public string NotifyUrl
        {
            get { return _notifyUrl; }
            set { _notifyUrl = value; }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //string filePath = @"C:/Users/admin/Desktop/log.txt";
            if (filterContext == null)
            {
               // File.AppendAllText(filePath, "context is null");
                throw new ArgumentNullException("filterContext");
            }

            if (AuthorizeCore(filterContext.HttpContext))
            {
                //File.AppendAllText(filePath, "AuthorizeCore");
                HttpCachePolicyBase cachePolicy =
                    filterContext.HttpContext.Response.Cache;
                cachePolicy.SetProxyMaxAge(new TimeSpan(0));
                //cachePolicy.AddValidationCallback(CacheValidateHandler, null);
            }

            /// This code added to support custom Unauthorized pages.
            else if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                //File.AppendAllText(filePath, "IsAuthenticated");
                if (NotifyUrl != null)
                {
                    //File.AppendAllText(filePath, "Notify Not Null");
                    filterContext.Result = new RedirectResult(NotifyUrl);
                }
                else
                {
                    //File.AppendAllText(filePath, "Notify is null");
                    // Redirect to Login page.
                    HandleUnauthorizedRequest(filterContext);

                }
            }
            /// End of additional code
            else
            {
                // Redirect to Login page.
                if(NotifyUrl != null)
                {
                    //File.AppendAllText(filePath, "Notify Not Null");
                    filterContext.Result = new RedirectResult(NotifyUrl);
                }
                else
                {
                    //File.AppendAllText(filePath, "Notify is null");
                    HandleUnauthorizedRequest(filterContext);
                }
                
            }
        }
    }

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}