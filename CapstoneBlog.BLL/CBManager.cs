﻿using CapstoneBlog.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapstoneBlog.Models;
using CapstoneBlog.Data.Utilities;

namespace CapstoneBlog.BLL
{
    public class CBManager : ICBManager
    {
        private readonly IRepository _repo;

        public CBManager(IRepository repo)
        {
            _repo = repo;
        }

        public List<Category> GetAllCategories()
        {
            List<Category> categories = new List<Category>();

            try
            {
                categories = _repo.GetAllCategories();
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
            }

            return categories;
        }

        public List<PostVM> GetAllDeletedPosts()
        {
            List<PostVM> posts = new List<PostVM>();

            try
            {
                posts = _repo.GetAllDeletedPosts();
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
            }

            return posts;
        }

        public List<PostVM> GetAllPostsForRevision()
        {
            List<PostVM> posts = new List<PostVM>();

            try
            {
                posts = _repo.GetAllPostsForRevision();
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
            }

            return posts;
            
        }

        public List<PostVM> GetAllPublicPosts()
        {
            List<PostVM> posts = new List<PostVM>();

            try
            {
                posts = _repo.GetAllPublicPosts();
            }

            catch (Exception e)
            {
                Logger.LogErrors(e);         
            }

            return posts;
        }

        public List<AspNetUser> GetAllUsersWithRole(int roleId)
        {
            List<AspNetUser> users = new List<AspNetUser>();

            try
            {
                users = _repo.GetAllUsersWithRole(roleId);
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
            }

            return users;

        }

        public List<PostVM> GetAuthorsPosts(string authorAuth)
        {
            List<PostVM> posts = new List<PostVM>();

            try
            {
                posts = _repo.GetAuthorsPosts(authorAuth);
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
            }

            return posts;


        }

        public Post GetPostById(int id)
        {
            Post post = new Post();

            try
            {
                post = _repo.GetPostById(id);
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
            }

            return post;
        }

        public PostVM GetPostVMById(int id)
        {
            PostVM post = new PostVM();

            try
            {
                post = _repo.GetPostVMById(id);
            }
            catch
            {
                //write to log
            }

            return post;
        }

        public List<PostVM> GetPostsWithCategories(List<string> categories)
        {
            List<PostVM> posts = new List<PostVM>();

            try
            {
                posts = _repo.GetPostsWithCategories(categories);
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
            }

            return posts;
        }

        public AspNetUser GetUserByAuth(string auth)
        {
            AspNetUser user = new AspNetUser();

            try
            {
                user = _repo.GetUserByAuth(auth);
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
            }

            return user;
        }

        public bool UsernameAvailable(string username)
        {
            bool result = false;

            try
            {
                if (_repo.UsernameAvailable(username))
                    result = true;
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
                result = false;
            }

            return result;
        }

        public bool AddPost(Post post)
        {
            bool result = false;
            try
            {
                _repo.AddPost(post);
                result = true;
            }

            catch (Exception e)
            {
                Logger.LogErrors(e);

                result = false;
            }

            return result;
        }

        public List<string> GetPostCategoriesAsStringList(string content)
        {
            if (content == null)
                return new List<string>();
            List<string> categories = new List<string>();
            foreach (string word in content.Split(' '))
            {
                if (word.Contains("#"))
                {
                    bool isCategory = false;
                    StringBuilder category = new StringBuilder();
                    foreach (char letter in word)
                    {
                        //Check to start new category
                        if (letter == '#')
                        {
                            if (!isCategory)
                                isCategory = true;
                            else
                            {
                                categories.Add(category.ToString());
                                category.Clear();
                            }
                        }
                        //Any other character
                        else
                        {
                            if (isCategory)
                            {
                                if (Char.IsLetter(letter))
                                {
                                    category.Append(letter);
                                }
                                else
                                {
                                    isCategory = false;
                                    categories.Add(category.ToString());
                                    category.Clear();
                                }
                            }
                        }
                    }
                    if (isCategory)
                    {
                        categories.Add(category.ToString());
                        category.Clear();
                    }
                }
            }
            return categories;
        }
        public List<Category> GetPostCategories(List<string> categories)
        {
            List<Category> newCategories = new List<Category>();
            foreach (string category in categories)
            {
                if (_repo.CategoryExists(category))
                {
                    newCategories.Add(_repo.GetCategory(category));
                }
                else
                {
                    _repo.AddCategory(category);
                    newCategories.Add(_repo.GetCategory(category));
                }
            }
            return newCategories;
        }

        public bool UpdatePost(PostVM post)
        {
            try
            {
                _repo.UpdatePost(post);
                return true;
            }
            catch(Exception e)
            {
                Logger.LogErrors(e);
                return false;
            }
        }

        public bool SetApproved(int postID)
        {
            try
            {
                _repo.SetApproved(postID);
                return true;
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
                return false;
            }
        }

        public bool SetRejected(int postID)
        {
            try
            {
                _repo.SetRejected(postID);
                return true;
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
                return false;
            }

        }

        public bool SetDeleted(int postID)
        {
            try
            {
                _repo.SetDeleted(postID);
                return true;
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
                return false;
            }
        }

        public List<PostVM> GetAllStaticPages()
        {
            List<PostVM> posts = new List<PostVM>();

            try
            {
                posts = _repo.GetAllStaticPages();
            }

            catch (Exception e)
            {
                Logger.LogErrors(e);
            }

            return posts;
        }

        public bool AddCategory(string category)
        {          
            try
            {
                _repo.AddCategory(category);
                return true;
            }

            catch (Exception e)
            {
                Logger.LogErrors(e);
                return false;
            }

        }

        public List<UserVM> GetAllUsers()
        {
            try
            {
                return _repo.GetAllUsers();
                
            }
            catch (Exception e)
            {
                Logger.LogErrors(e);
                return new List<UserVM>();
            }

        }

        public bool UpdateUserRole(string id, string role)
        {
            try
            {
                _repo.UpdateUserRole(id, role);
                return true;
            }
            catch(Exception e)
            {
                Logger.LogErrors(e);
                return false;
            }

        }

    }
}
