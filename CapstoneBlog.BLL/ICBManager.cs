﻿using CapstoneBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneBlog.BLL
{
    public interface ICBManager
    {
        List<Category> GetAllCategories();
        List<PostVM> GetAllDeletedPosts();
        List<PostVM> GetAllPostsForRevision();
        List<PostVM> GetAllPublicPosts();
        List<AspNetUser> GetAllUsersWithRole(int roleId);
        List<PostVM> GetAuthorsPosts(string authorAuth);
        Post GetPostById(int id);
        PostVM GetPostVMById(int id);
        List<PostVM> GetPostsWithCategories(List<string> categories);
        List<PostVM> GetAllStaticPages();
        AspNetUser GetUserByAuth(string auth);
        bool UsernameAvailable(string username);
        List<string> GetPostCategoriesAsStringList(string content);
        List<Category> GetPostCategories(List<string> categories);
        bool AddPost(Post post);
        bool UpdatePost(PostVM post);
        bool SetApproved(int postID);
        bool SetRejected(int postID);
        bool SetDeleted(int postID);
        bool AddCategory(string category);
        List<UserVM> GetAllUsers();
        bool UpdateUserRole(string id, string role);

    }
}
