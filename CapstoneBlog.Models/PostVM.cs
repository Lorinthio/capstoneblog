﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CapstoneBlog.Models
{
    public class PostVM
    {
        //Creating
        public int PostID { get; set; }
        [Required]
        public string Title { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Expiration { get; set; }
        [Required]
        public string PostContent { get; set; }

        //Displaying
        public string UserName { get; set; } = "";
        public DateTime Date { get; set; }
        public List<string> Categories;
        public List<Comment> Comments;
        public bool isApproved;
        public bool isDeleted;
        public bool isRejected;
        public bool isStaticPage { get; set; }

        public PostVM() { }

        public PostVM(Post post)
        {
            if(post != null)
            {
                this.PostID = post.Post_ID;
                this.Date = post.Date;
                this.Title = post.Title;
                this.PostContent = post.PostContent;
                this.UserName = post.AspNetUser.UserName;
                this.Expiration = post.Expiration;
                this.Categories = (from category in post.Categories
                                   select category.Category1).ToList();
                this.Comments = post.Comments.ToList();
                this.isApproved = post.isApproved;
                this.isDeleted = post.isDeleted;
                this.isRejected = post.isRejected;
                this.isStaticPage = post.isStaticPage;
            }
        }

        public TimeSpan GetAge()
        {
            return Date.Subtract(DateTime.Now);
        }

        public TimeSpan GetRemainingTime()
        {
            if(Expiration != null)
            {
                return Expiration.GetValueOrDefault().Subtract(DateTime.Now);
            }
            return new TimeSpan();
        }

        public string GetRaw()
        {
            return System.Text.RegularExpressions.Regex.Replace(PostContent, "<[^>]*>", "");
        }
    }
}