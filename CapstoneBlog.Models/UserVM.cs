﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneBlog.Models
{
    public class UserVM
    {

        public string UserName { get; set; }
        public string AuthID { get; set; }
        public string Role { get; set; }
        
    }
}
